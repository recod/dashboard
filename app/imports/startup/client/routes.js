import {FlowRouter} from "meteor/kadira:flow-router";
import {BlazeLayout} from "meteor/kadira:blaze-layout";
import "../../ui/layouts/body/body.js";
import "../../ui/pages/home/home.js";
import "../../ui/pages/lab/lab.js";
import "../../ui/pages/not-found/not-found.js";

// Set up all routes in the app
FlowRouter.route('/', {
    name: 'App.home',
    action() {
        BlazeLayout.render('App_body', {main: 'App_home'});
    },
});

FlowRouter.route('/lab', {
    name: 'App.lab',
    action() {
        BlazeLayout.render('App_body', {main: 'App_lab'});
    },
});

FlowRouter.notFound = {
    action() {
        BlazeLayout.render('App_body', {main: 'App_notFound'});
    },
};
