import {Meteor} from "meteor/meteor";
import {Machines} from '../machines';

Meteor.publish('monitor.machines.all', function () {
    return Machines.find();
});
