import {Meteor} from "meteor/meteor";
import {Machines} from "../machines";


Meteor.startup(() => {
    Machines._ensureIndex({label: 1}, {unique: 1});
});
