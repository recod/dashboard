import {Mongo} from "meteor/mongo";

export const Machines = new Mongo.Collection('monitor.machines');

Machines.deny({
    insert() {
        return true;
    },
    update() {
        return true;
    },
    remove() {
        return true;
    },
});