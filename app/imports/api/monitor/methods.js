import {Meteor} from "meteor/meteor";
import {sync} from './synchronizer';


Meteor.methods({
    'monitor.sync.start'() {
        sync.start();
    },
});
