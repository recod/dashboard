import {Meteor} from 'meteor/meteor';
import {Machines} from './machines/machines';


export const API_REFRESH_RATE = 1000,
             MAX_SYNC_CYCLES = 60;


class Synchronizer {
    constructor() {
        this.taskId = null;
        this.status = 'stopped';
        this.syncCycles = 0;
    }

    sync() {
        if (this.status === 'running') {
            try {
                const response = HTTP.get(Meteor.settings.RecodAPI + '/monitor/machines');
                _.each(response.data, (item) => Machines.update({label: item.label}, {$set: item}, {upsert: true}));
            } catch (error) {
                console.log(error);
            }
        }

        this.syncCycles++;
        if (this.syncCycles >= MAX_SYNC_CYCLES) {
            this.stop();
        }
    }

    start() {
        const self = this;

        if (this.status === 'stopped') {
            self.status = 'running';
            self.taskId = Meteor.setInterval(() => self.sync(), API_REFRESH_RATE);
        }

        self.syncCycles = 0;
    }

    stop() {
        if (this.status === 'running') {
            this.status = 'stopped';
            Meteor.clearInterval(this.taskId);
            this.taskId = null;
        }
    }
}

export const sync = new Synchronizer();
