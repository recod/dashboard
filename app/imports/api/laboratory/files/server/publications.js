import {Meteor} from "meteor/meteor";
import {Files} from "../files.js";

Meteor.publish('laboratory.files.all', function () {
    return Files.find();
});
