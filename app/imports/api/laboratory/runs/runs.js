import {Mongo} from "meteor/mongo";
import {Index, MinimongoEngine} from "meteor/easy:search";

export const Runs = new Mongo.Collection('default.runs');

Runs.deny({
    insert() {
        return true;
    },
    update() {
        return true;
    },
    remove() {
        return true;
    },
});

export const RunsIndex = new Index({
    collection: Runs,
    fields: ['experiment.name', 'status'],
    defaultSearchOptions: {
        limit: 20
    },
    engine: new MinimongoEngine({
        sort: function () {
            return {
                archived: 1,
                start_time: -1,
            }
        },
    }),
});
