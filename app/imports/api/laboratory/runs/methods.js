import {Meteor} from "meteor/meteor";
import {Runs} from "./runs.js";

Meteor.methods({
    'laboratory.runs.archive'({runId, archived}) {
        return Runs.update(runId, {$set: {archived: Boolean(archived)}});
    },
});
