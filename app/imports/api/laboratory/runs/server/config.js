import {Meteor} from "meteor/meteor";
import {Runs} from "../runs";


Meteor.startup(() => {
    Runs._ensureIndex({archived: 1});
});
