import {Meteor} from "meteor/meteor";
import {Runs} from "../runs.js";

Meteor.publish('laboratory.runs.active', function () {
    return Runs.find({archived: {$ne: true}});
});

Meteor.publish('laboratory.runs.all', function () {
    return Runs.find();
});
