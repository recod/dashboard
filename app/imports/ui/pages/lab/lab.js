import './lab.html';

import '../../components/laboratory/runs/runs.js';

Template.App_lab.onCreated(function App_labOnCreated() {
    this.leadSentences = [
        'Cake, and grief counseling, will be available at the conclusion of the test.',
        'Please note that we have added a consequence for failure. Any contact with the chamber floor will result in an unsatisfactory mark on your official testing record, followed by death.',
        'Remember when the platform was sliding into the fire pit and I said "Goodbye" and you were like [in a deep male voice] "No way!" [normal voice] And then I was all "We pretended we were going to murder you?" That was great!',
        'This is your fault. I\'m going to kill you. And all the cake is gone. You don\'t even care, do you?',
        'Momentum, a function of mass and velocity, is conserved between portals. In layman\'s terms, speedy thing goes in, speedy thing comes out.',
        'Area and state regulations do not allow the Companion Cube to remain here, alone and companionless.',
        'As part of a required Enrichment Center protocol, the previous statement that we would not monitor the test area was a complete fabrication. We will stop enhancing the truth in three... two... *zzzt*',
        'The Enrichment Center promises to always provide a safe testing environment. In dangerous testing environments, the Enrichment Center promises to always provide useful advice. For instance: the floor here will kill you. Try to avoid it.',
        'Well done. Here come the test results: "You are a horrible person." That\'s what it says: a horrible person. We weren\'t even testing for that.',
        'These bridges are made from natural light that I pump in from the surface. If you rubbed your cheek on one, it would be like standing outside with the sun shining on your face. It would also set your hair on fire, so don\'t actually do it.',
        'Did you know that people with guilty consciences are more easily startled by loud noises? [train horn] I\'m sorry, I don\'t know why that went off. Anyway, just an interesting science fact.',
        'That jumpsuit you\'re wearing looks stupid. That\'s not me talking, it\'s right here in your file. On other people it looks fine, but right here a scientist has noted that on you it looks "stupid". Well, what does a neck-bearded old engineer know about fashion? He probably – Oh, wait. It\'s a she. Still, what does she know? Oh wait, it says she has a medical degree. In fashion! From France!',
        'Oh, thank God you\'re all right. You know, being Caroline taught me a valuable lesson. I thought you were my greatest enemy, but all along you were my best friend. The surge of emotion that shot through me when I saved your life taught me an even more valuable lesson: where Caroline lives in my brain. [Announcer: "Caroline deleted."] Goodbye, Caroline. You know, deleting Caroline just now taught me a valuable lesson. The best solution to a problem is usually the easiest one. And I\'ll be honest - killing you is hard. You know what my days used to be like? I just tested. Nobody murdered me, or put me in a potato, or fed me to birds. I had a pretty good life. And then you showed up. You dangerous, mute lunatic. So you know what? You win. Just go. [chuckles] It\'s been fun. Don\'t come back.',
    ];
});

Template.App_lab.helpers({
    randomLeadSentence() {
        const sentences = Template.instance().leadSentences;
        return sentences[Math.floor(Math.random() * sentences.length)];
    }
});
