import {Meteor} from "meteor/meteor";
import {RunsIndex} from "../../../../api/laboratory/runs/runs";
import {COLORS, ICONS} from "./consts";
import "./runs.css";
import "./runs.html";

Template.runs.onCreated(function laboratoryOnCreated() {
    Meteor.subscribe('laboratory.runs.all');
});

Template.runs.onRendered(function laboratoryOnRendered() {
    $(document).ready(function () {
        $('.collapsible').collapsible();
    });
});


Template.runs.helpers({
    stringify: JSON.stringify,
    localDate: (e) => e ? e.toLocaleString() : '?',
    timeTaken: (start, end, heartbeat) => {
        return start && end
            ? Math.round(((end - start % 86400000) % 3600000) / 60000)
            : Math.round(((heartbeat - start % 86400000) % 3600000) / 60000);
    },
    iconForStatus: (icon) => ICONS[icon],
    colorForStatus: (icon) => COLORS[icon],
    lower: (e) => e.toLowerCase(),
    prettyScore: (number) => Math.round(100 * number),
    cleanFailTrace: (trace) => trace.join(''),
    hasInfo: (run) => run.info && !$.isEmptyObject(run.info),
    isRunning: (run) => run.status === 'RUNNING',
    runsIndex: () => RunsIndex,
    inputAttributes: () => { return {id: 'search', name: 'search', type: 'text', placeholder: 'search'}; },
    loadMoreBtnAttributes: () => { return {'class': 'btn'}; },
});

Template.runs.events({
    'click .archive-run'() {
        Meteor.call('laboratory.runs.archive', {
            runId: this._id, archived: !this.archived
        });
        return false;
    }
});
