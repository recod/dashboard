export const ICONS = {
    'FAILED': 'error',
    'INTERRUPTED': 'warning',
    'COMPLETED': 'done',
    'RUNNING': 'directions_run',
};

export const COLORS = {
    'FAILED': 'red',
    'INTERRUPTED': 'grey',
    'COMPLETED': 'green',
    'RUNNING': 'blue',
};
