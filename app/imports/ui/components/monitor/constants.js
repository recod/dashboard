export const COLORS = ['blue', 'green', 'yellow darken-2', 'orange', 'red'];
export const RESOURCES = {
    cpu_and_memory: 'CPUs and Memory',
    video: 'GPUs',
    users: 'Users',
    docker: 'Docker Containers',
};
export const ATTRIBUTES = {
    id: 'Id',
    name: 'Name',
    mem_total: 'Total memory',
    mem_util: 'Used memory',
    running_tasks: 'Running tasks',
    utilization: 'Utilization',
    temperature: 'Temperature',
    fan_util: 'Fan Utilization',
    username: 'Username',
    what: 'What',
    tty: 'TTY',
    idle: 'Idle',
    from: 'From',
};
export const NAMES = {
    ldavid: 'Lucas David',
    rwerneck: 'Rafael Werneck',
    piresran: 'Ramon Pires',
    kbogdan: 'Karina Bogdan',
};
