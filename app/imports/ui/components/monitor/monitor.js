import {Meteor} from "meteor/meteor";
import {Template} from "meteor/templating";
import {Machines} from "../../../api/monitor/machines/machines";
import {RESOURCES, ATTRIBUTES, NAMES, COLORS} from "./constants";
import "./monitor.css";
import "./monitor.html";


Template.monitor.onCreated(function monitorOnCreated() {
    Meteor.subscribe('monitor.machines.all');
    Meteor.call('monitor.sync.start');

    this.taskSyncId = Meteor.setInterval(() => Meteor.call('monitor.sync.start'), 60 * 1000);
});

Template.monitor.onDestroyed(function monitorOnDestroy() {
    Meteor.clearInterval(this.taskSyncId);
});

Template.monitor.helpers({
    not: (e) => !e,

    prettyResource: (label) => RESOURCES[label],
    prettyResourceAttribute: (label) => ATTRIBUTES[label] || label,
    nameFromUsername: (username) => NAMES[username] || username,
    fixed1: (value) => value.toFixed(1),
    initialsFromUsername: (username) => {
        const name = NAMES[username];

        if (name) {
            const parts = name.split(' ');
            return [parts[0][0], parts[parts.length - 1][0]]
                .join('')
                .toUpperCase();
        } else {
            return username.slice(0, 2).toUpperCase();
        }
    },
    describe: (obj) => {
        const result = [];
        for (let k in obj) result.push({key: k, value: obj[k]});
        return result;
    },

    status: () => Template.instance().status.get(),
    updating: () => Template.instance().updating.get(),
    machines: () => Machines.find(),
    usableResources(machine) {
        if (!machine || !machine.info.resources) return {};

        let result = [];
        for (let resource of machine.info.resources)
            if (resource.label != 'users' && resource.label != 'docker')
                result.push(resource);
        return result;
    },
    loggedUsers(machine) {
        for (let resource of machine.info.resources) {
            if (resource.label == 'users') return resource.instances;
        }
    },
    activeContainers(machine) {
        for (let resource of machine.info.resources) {
            if (resource.label == 'docker') return resource.instances;
        }
    },
    color(utilization) {
      if (utilization === undefined) {
        utilization = 0;
      }

      const c = Math.min(Math.floor(COLORS.length * utilization/100),
                         COLORS.length-1);
      return COLORS[c];
    }
});


Template.monitor.events({
    'click .toggle-pause-btn'(event, instance) {
        instance.status.set(instance.status.get() == 'sync'
            ? 'sync_disabled'
            : 'sync'
        );
    },
});
