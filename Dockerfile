FROM geoffreybooth/meteor-base:1.7.0.3

COPY ./app/package*.json $APP_SOURCE_FOLDER/
RUN bash $SCRIPTS_FOLDER/build-app-npm-dependencies.sh
COPY ./app $APP_SOURCE_FOLDER/
RUN bash $SCRIPTS_FOLDER/build-meteor-bundle.sh


FROM node:8-alpine

ENV APP_BUNDLE_FOLDER /opt/bundle
ENV SCRIPTS_FOLDER /docker

RUN apk --no-cache --virtual .node-gyp-compilation-dependencies add \
		g++ \
		make \
		python \
	&& apk --no-cache add \
		bash \
		ca-certificates

COPY --from=0 $SCRIPTS_FOLDER $SCRIPTS_FOLDER/

COPY --from=0 $APP_BUNDLE_FOLDER/bundle $APP_BUNDLE_FOLDER/bundle/

RUN bash $SCRIPTS_FOLDER/build-meteor-npm-dependencies.sh \
	&& apk del .node-gyp-compilation-dependencies

ENTRYPOINT ["/docker/entrypoint.sh"]

CMD ["node", "main.js"]

