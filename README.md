# Dashboard

Basic client application for RECOD lab.

## Setup

Install meteor and npm first:

```shell
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install nodejs build-essential

curl https://install.meteor.com/ | sh
```

### Development

Make sure the [RecodAPI](https://bitbucket.org/recod/api) is running and
the endpoint specified at `settings-dev.json` correctly directs to it.

```shell
git clone git@bitbucket.org:recod/dashboard.git
cd dashboard/app
meteor npm install
meteor --settings ../settings/dev.json
```

## Deployment

```shell
ssh recodash@10.0.1.218

git clone git@bitbucket.org:recod/dashboard.git
cd repos/recod/dashboard
./build.sh

cp deploy/dashboard-nginx /etc/nginx/sites-enabled/dashboard

sudo nginx service restart
```

Lastly, make sure nginx config file `recod-services.config` is in place.
See [settings-and-info](https://bitbucket.org/recod/settings-and-info) repository
for more details.

Further deployments can be done by building and reloading the app with the
`passenger-config restart-app /home/recodash/repos/recod/dashboard/build/bundle` command.

If something went wrong, or you simply want to deploy it onto a different server, follow this tutorial:
[phusionpassenger.com/.../deploy/meteor/](https://www.phusionpassenger.com/library/walkthroughs/deploy/meteor/).
